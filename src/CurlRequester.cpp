#include "CurlRequester.h"
#include <sys/stat.h>
#include <functional>


namespace {
	size_t write_callback(char *ptr, size_t size, size_t nmemb, void *userdata){
		std::string &dst = *reinterpret_cast<std::string*>(userdata);
		dst.append(ptr, size*nmemb);
		return size*nmemb;
	}
	// Needed since curl_global_* functions are not threadsafe.
	// CurlDestroyer instantiated statically once in Requester constructor
	// and curl_global_cleanup is called when the module is unloaded.
	class CurlDestructor{
	public:
		~CurlDestructor(){ curl_global_cleanup(); }
	};
	void init_curl()
	{
		static CURLcode once = curl_global_init(CURL_GLOBAL_ALL);
		static CurlDestructor destructor;
	}
}

CurlRequester::CurlRequester() :
	m_curl(nullptr, &curl_easy_cleanup),
	m_headers(nullptr, &curl_slist_free_all),
	m_timeout(300*1000),
	m_maxBytesPerSecond(0)
{
	init_curl();
}
CurlRequester::~CurlRequester(){}

CurlResult::CurlResult() :
	m_responseCode(0),
	m_curlCode(CURLE_OK)
{
}
CurlResult::CurlResult(long responseCode, const std::string &content, CURLcode curlCode) :
m_responseCode(responseCode),
m_content(content),
m_curlCode(curlCode)
{
}

long CurlResult::responseCode() const 
{
	return m_responseCode;
}
std::string CurlResult::responseContent() const 
{
	return m_content;
}
CURLcode CurlResult::curlCode() const 
{
	return m_curlCode;
}

size_t CurlRequester::ftp_read_callback(void *ptr, size_t size, size_t nmemb, void *userp){
	static time_t lastRead = 0;
	time_t currentTime = time(NULL);
	time_t timeElapsed = currentTime - lastRead;

	//compute the maximum number of bytes permitted in a single cycle.
	int milliseconds = 1000 * nmemb / m_maxBytesPerSecond;
	int sleepTime = nmemb >= m_maxBytesPerSecond ? 1000 : milliseconds;
	if (timeElapsed < 1) {
		Sleep(sleepTime);
	}

	//cast user pointer to FILE*
	FILE* hd_src = (FILE*)userp;
	if (size*nmemb < 1) { //invalid byte count
		return 0;
	}
	if (!feof(hd_src)){
		size_t currentBytesRead = fread(ptr, size, nmemb, hd_src);
		lastRead = currentTime;
		return currentBytesRead;
	}

	return 0;
}

void CurlRequester::set_timeout_ms(long timeout)
{
	m_timeout = timeout;
}
void CurlRequester::set_max_send_speed(long bytes_per_second)
{
	m_maxBytesPerSecond = bytes_per_second;
}
void CurlRequester::reset_handle()
{
	m_curl.reset(curl_easy_init());
	curl_easy_setopt(m_curl.get(), CURLOPT_HTTPHEADER, m_headers.get());
	curl_easy_setopt(m_curl.get(), CURLOPT_MAX_SEND_SPEED_LARGE, m_maxBytesPerSecond);
	curl_easy_setopt(m_curl.get(), CURLOPT_TIMEOUT_MS, m_timeout);
	curl_easy_setopt(m_curl.get(), CURLOPT_WRITEFUNCTION, write_callback);
	curl_easy_setopt(m_curl.get(), CURLOPT_FAILONERROR, 1);
}
CurlResult CurlRequester::get_url(const std::string &url)
{
	CurlResult result;
	reset_handle();
	curl_easy_setopt(m_curl.get(), CURLOPT_URL, url.c_str());
	curl_easy_setopt(m_curl.get(), CURLOPT_WRITEDATA, &result.m_content);
	result.m_curlCode = curl_easy_perform(m_curl.get());
	curl_easy_getinfo(m_curl.get(), CURLINFO_RESPONSE_CODE, &result.m_responseCode);
	return result;
}

CurlResult CurlRequester::post_url(const std::string &url,  const std::string &postfields)
{
	CurlResult result;
	reset_handle();
	curl_easy_setopt(m_curl.get(), CURLOPT_URL, url.c_str());
	curl_easy_setopt(m_curl.get(), CURLOPT_POSTFIELDS, postfields.c_str());
	curl_easy_setopt(m_curl.get(), CURLOPT_POSTFIELDSIZE, postfields.length());
	curl_easy_setopt(m_curl.get(), CURLOPT_WRITEDATA, &result.m_content);
	result.m_curlCode = curl_easy_perform(m_curl.get());
	curl_easy_getinfo(m_curl.get(), CURLINFO_RESPONSE_CODE, &result.m_responseCode);
	return result;
}


void CurlRequester::set_headers(const std::vector<std::string> &headers)
{
	if (headers.size()){
		auto start = headers.begin();
		m_headers.reset(curl_slist_append(nullptr, start->c_str()));
		while (++start != headers.end()){
			curl_slist_append(m_headers.get(), start->c_str());
		}
	}
}

CurlResult CurlRequester::upload_ftp(const std::string &hostname, const std::string &file)
{
	CurlResult result;
	struct stat file_info;
	curl_off_t fsize;

	reset_handle();
	std::unique_ptr<FILE, decltype(&fclose)> hd_src(fopen(file.c_str(), "rb"), &fclose);
	if (!hd_src){
		result.m_curlCode = CURLE_READ_ERROR;
	} else {
		// get the file size of the local file
		if(stat(file.c_str(), &file_info)) {
			result.m_curlCode = CURLE_READ_ERROR;
			return result;
		}

		// Set the size of the file to upload (optional).  If you give a *_LARGE
		//   option you MUST make sure that the type of the passed-in argument is a
		//   curl_off_t. If you use CURLOPT_INFILESIZE (without _LARGE) you must
		//   make sure that to pass in a type 'long' argument.
		fsize = (curl_off_t)file_info.st_size;
		curl_easy_setopt(m_curl.get(), CURLOPT_INFILESIZE_LARGE,(curl_off_t)fsize);

		// specify which file to upload
		curl_easy_setopt(m_curl.get(), CURLOPT_READDATA, hd_src.get());

		//specify the read callback function
		curl_easy_setopt(m_curl.get(), CURLOPT_READFUNCTION, std::bind(&CurlRequester::ftp_read_callback, this));
		curl_easy_setopt(m_curl.get(), CURLOPT_BUFFERSIZE, 1024);

		// enable uploading
		curl_easy_setopt(m_curl.get(), CURLOPT_UPLOAD, 1L);

		// specify the max data upload speed (bytes per second)
		curl_off_t limit = (curl_off_t)m_maxBytesPerSecond;
		curl_easy_setopt(m_curl.get(), CURLOPT_MAX_SEND_SPEED_LARGE, limit);

		// data transfer should timeout if it takes longer than 5 min
		curl_easy_setopt(m_curl.get(), CURLOPT_TIMEOUT, 300L);

		// Now run off and do what you've been told!
		result.m_curlCode = curl_easy_perform(m_curl.get());
	}
	//close the file handle
	return result;
}

std::string CurlRequester::curlcode_to_string(CURLcode code){
	return curl_easy_strerror(code);
}
