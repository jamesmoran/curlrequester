#pragma once

#include <string>
#include <map>
#include <vector>
#include <memory>
#include "curl/curl.h"
#include "CoreDefines.h"

class CurlRequester;
class CurlResult {
	friend CurlRequester;
public:
	MMI_EXPORT CurlResult();
	MMI_EXPORT CurlResult(long responseCode, const std::string &content, CURLcode curlCode);
	
	MMI_EXPORT long responseCode() const;
	MMI_EXPORT CURLcode curlCode() const;
	MMI_EXPORT std::string responseContent() const;

protected:
	long m_responseCode;
	CURLcode m_curlCode;
	std::string m_content;
};

class CurlRequester
{
public:
	MMI_EXPORT CurlRequester();
	MMI_EXPORT virtual ~CurlRequester();

	MMI_EXPORT virtual CurlResult get_url(const std::string &url);
	MMI_EXPORT virtual CurlResult post_url(const std::string &url,  const std::string &postfields);
	MMI_EXPORT virtual CurlResult upload_ftp(const std::string &hostname, const std::string &file);

	MMI_EXPORT virtual void set_headers(const std::vector<std::string> &headers);
	MMI_EXPORT virtual void set_timeout_ms(long timeout);
	MMI_EXPORT virtual void set_max_send_speed(long bytes_per_second);
	MMI_EXPORT static std::string curlcode_to_string(CURLcode code);

private:
	void reset_handle();
	size_t ftp_read_callback(void *ptr, size_t size, size_t nmemb, void *userp);
	std::unique_ptr<CURL, decltype(&curl_easy_cleanup)> m_curl;
	std::unique_ptr<curl_slist, decltype(&curl_slist_free_all)> m_headers;
	long m_timeout;
	int m_maxBytesPerSecond;

};
